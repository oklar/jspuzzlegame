class Grid {
    constructor ({xStartPosition = 0, yStartPosition = 0, xGridLength = 0, yGridLength = 0, gridWidth = 0, gridHeight = 0}) {
        this._x = xStartPosition;
        this._y = yStartPosition;
        this._xLength = xGridLength;
        this._yLength = yGridLength;
        this._width = gridWidth;
        this._height = gridHeight;

        this.grid = [];
        this.id = 0;

        this.blockFrames = [0, 1, 2, 3, 8, 10];
        //this.unmatchableFrame = [10];

        // Usually powerups
        this.oneClickBlockFrame = [10];

    }

    createGrid(scene, data, level) {
        level -= 1;
        this.blockPercentage = this.setRandomBlock(data.level[level].block.percentage);
        let dataGrid = data.level[level].grid.setup;
        let dataGridLenght = data.level[level].grid.setup.length;
        this._xLength = data.level[level].grid.width;
        this._yLength = data.level[level].grid.height;

        for (let i = 0; i < this._xLength; i++)
        {
            this.grid[i] = [];
            for (let j = 0; j < this._yLength; j++)
            {
                let sx = this._x + (i * this._width);
                let sy = this._y + (j * this._height);

                if (dataGridLenght !== 0)
                    this.grid[i][j] = this.createBlockOnGrid(i, j, sx, sy, scene, dataGrid[i][j]);
                else
                    this.grid[i][j] = this.createBlockOnGrid(i, j, sx, sy, scene)

            }
        }
        return this;
    }

    resetLevel() {
        for (let i = 0; i < this.xLength; i++) {
            for (let j = 0; j < this.yLength; j++) {
                let b = this.grid[i][j];
                if (b != null && b !== undefined) {
                    b.destroy();
                }
                this.grid[i][j] = undefined;
            }
        }
    }

    randomBlock() {
        let r = Phaser.Math.Between(0, 99);
        this.blockPercentage.sort(function(a, b){return a.value > b.value});

        let sum = 0;
        for (let i = 0; i < this.blockPercentage.length; i++) {
            sum += Math.round(this.blockPercentage[i].value);
            if (r < sum) {
                return this.blockPercentage[i].frame;
            }
        }
        // this shouldn't happen but if it does let's return a random block
        console.error("randomBlock error");
        return this.blockFrames[Phaser.Math.Between(0, this.blockFrames.length-1)];
    }


    getRandomLeftOvers(leftOver) {
        let sum = 0;
        for (let i = 0; i < leftOver.length; i++) {
            sum += leftOver[i];
        }
        return 100 - sum;
    }

    setRandomBlock({ isEqual = true, blue = 0, green = 0, orange = 0, red = 0, stone = 0, move = 0 } = {}) {
        if (isEqual) {
            let len = this.blockFrames.length;
            return [
                {key : 'blue', value : 100/len, frame : this.blockFrames[0]},
                {key : 'green', value : 100/len, frame : this.blockFrames[1]},
                {key : 'orange', value : 100/len, frame : this.blockFrames[2]},
                {key : 'red', value : 100/len, frame : this.blockFrames[3]},
                {key : 'stone', value : 100/len, frame : this.blockFrames[4]},
                {key : 'move', value : 100/len, frame : this.blockFrames[5]},
            ];

        } else {
            let sumLeftOvers = this.getRandomLeftOvers(Object.values(arguments[0]));
            return [
                {key : 'blue',
                    value : (sumLeftOvers <= 0 ? blue : blue + (blue/(100 - sumLeftOvers) * sumLeftOvers)),
                    frame : this.blockFrames[0]},
                {key : 'green',
                    value : (sumLeftOvers <= 0 ? green : green + (green/(100 - sumLeftOvers) * sumLeftOvers)),
                    frame : this.blockFrames[1]},
                {key : 'orange',
                    value : (sumLeftOvers <= 0 ? orange : orange + (orange/(100 - sumLeftOvers) * sumLeftOvers)),
                    frame : this.blockFrames[2]},
                {key : 'red',
                    value : (sumLeftOvers <= 0 ? red : red + (red/(100 - sumLeftOvers) * sumLeftOvers)),
                    frame : this.blockFrames[3]},
                {key : 'stone',
                    value : (sumLeftOvers <= 0 ? stone : stone + (stone/(100 - sumLeftOvers) * sumLeftOvers)),
                    frame : this.blockFrames[4]},
                {key : 'move',
                    value : (sumLeftOvers <= 0 ? move : move + (move/(100 - sumLeftOvers) * sumLeftOvers)),
                    frame : this.blockFrames[5]}
            ];
        }
    }

    createBlockOnGrid(x, y, sx, sy, scene, dataFrame) {
        let frame;

        if (dataFrame !== undefined)
        {
            frame = dataFrame;
        } else
        {
            frame = this.randomBlock();
        }


        let block = scene.add.sprite(sx, sy, 'blocks', frame);

        block.setInteractive();
        block.setData('frame', frame);
        block.setData('x', sx);
        block.setData('y', sy);
        block.setData('gx', x);
        block.setData('gy', y);
        block.setData('id', this.id++);

        //block.on('clicked', scene.clickHandler, scene);
        scene.setScaleRatio(block, config.scaleRatio);

        return block;
    }

    getColumnLength(column) {
        if (column >= this._xLength) console.error(`Column (${column}) is out of bounds, must be less then ${this._xLength}`);

        let length = -1;
        for(let i = 0; i < this._yLength; i++) {
            if (this.grid[column][i] != null && this.grid[column][i] !== undefined )  length++;
        }
        return length;
    }

    getLastItem(column) {
        return this.getBlock(column, this.getColumnLength(column));
    }

    checkAddRow(sceneObject) {
        if (sceneObject === undefined) console.error(this.getErrorMessage(0)); else {
            let len = this.getHighestLength() + 1;
            while (len < this._yLength) {
                this.addRow(sceneObject);
                len++;
            }
        }
    }

    getHighestLength() {
        let highest = -1;
        for (let i = 0; i < this.xLength; i++) {
            let b = this.getLastItem(i);
            if (b != null && b !== undefined) {
                let yBlock = b.getData('gy');
                if (yBlock > highest)
                    highest = yBlock;
            }
        }
        return highest;
    }

    addRow(sceneObject) {
        if (sceneObject === undefined) console.error(this.getErrorMessage(0)); else {
            this.moveRows(sceneObject, 1);
            // addd a new row add the beginning
            for (let column = 0; column < this._xLength; column++) {
                let b = this.createBlockOnGrid(column, 0, this._x + (column * this.width), -this._height, sceneObject);
                this.grid[column][0] = b;
                sceneObject.setScaleRatio(b, config.scaleRatio);

                this.tween(b, {
                    sceneObject: sceneObject,
                    duration: 700,
                    ease: 'Bounce.easeOut',
                    toX: b.getData('x'),
                    toY: this._y
                });
            }
        }
    }

    /**
     * Move all the rows
     * @param sceneObject: Phaser.Scene
     * @param amount: How far too move them
     */
    moveRows(sceneObject, amount) {
        for (let column = 0; column < this._xLength; column++) {
            this.moveRow(sceneObject, column, amount);
        }
    }

    /**
     * Move a single row
     * @param sceneObject: Phaser.Scene
     * @param amount: How far too move them
     * @param column: Which column on the grid
     */
    moveRow(sceneObject, column, amount) {
        // Move each column amount down beginning from the top
        for (let i = this.getColumnLength(column); i > -1; i--) {
            let b = this.grid[column][i];
            if (b != null && b !== undefined) {
                b.setData('y', this._y + (i * this.height) + (amount * this.height));
                this.grid[column][b.getData('gy')] = this.grid[column][b.getData('gy') - amount];
                b.setData('gy', b.getData('gy') + amount);
                this.grid[column][b.getData('gy')] = b;
                this.tween(b, {
                    sceneObject: sceneObject,
                    duration: 700,
                    ease: 'Bounce.easeOut',
                    toX: b.getData('x'),
                    toY: b.getData('y')
                });
            }

        }
    }

    removeBlock(gx, gy, {sceneObject, toX = 0, toY = 0, ease = 'Quart.easeOut', duration = 1500}) {
        let b = this.grid[gx][gy];
        // We also need to remove blocks which are below the removed block
        // Find block column height and compare it to the highest height of the column
        let len = this.getColumnLength(gx);

        // To get the correct length we need to set it undefined afterwards
        this.grid[gx][gy] = undefined;

        while (len > gy) {
            gy++;
            let bb = this.grid[gx][gy];
            // recursively remove the blocks
            if (bb != null && bb !== undefined) {
                this.uponBlockDeletion(sceneObject, bb);
                /*this.removeBlock(gx, gy, {
                    sceneObject: sceneObject,
                    toX: toX,
                    toY: toY + this.height,
                    ease: ease,
                    duration: duration
                });*/
            }
            this.grid[gx][gy] = undefined;
        }

        if (sceneObject !== undefined) {
            if (b != null && b !== undefined) {
                this.uponBlockDeletion(sceneObject, b);
                /*this.tween(b, {
                    sceneObject: sceneObject,
                    toX: toX,
                    toY: toY,
                    ease: ease,
                    duration: duration
                });*/
            }
        }
        return b;
    }

    uponBlockDeletion(sceneObject, bb) {
        // Gets the grid relative position
        //var block = sceneObject.physics.add.image(bb.getData('x'), this._y + (bb.getData('gy') * this.height), 'blocks', bb.getData('frame'));

        // Gets the actual y position of the to be deleted block
        var block = sceneObject.physics.add.image(bb.getData('x'), bb.y, 'blocks', bb.getData('frame'));

        bb.destroy();
        sceneObject.setScaleRatio(block, config.scaleRatio);
        block.setBounce(1, 1);
        block.setCollideWorldBounds(true);
        sceneObject.time.addEvent({ delay: Phaser.Math.Between(800, 1300), callback: function() {
            this.destroy();
            }, callbackScope: block });
    }

    belongsTo(block) {
        for(let i = 0; i < this._xLength; i++) {
            if (this.grid[i].includes(block)) {
                return true;
            }
        }
        return false;
    }

    getSelectable(scene, grid) {
        let selection = [];
        for (let x = 0; x < grid.xLength; x++) {
            let last = grid.getLastItem(x);
            if (last !== undefined) selection.push(last);
        }

        let allBlocksInGrid = this.getAllBlocks(grid);
        if (!this.isAtleastOneColumn(grid) || !this.isMatchingBlock(allBlocksInGrid)) {
            console.log("There's 1 column populated on the grid or there's no more blocks to match");
            console.log("IE: you've lost");
            return [];
        } else if (this.isMatchingBlock(selection)) {
            console.log("There's is matchable blocks");
            return selection;
        } else {
            // swap all blocks and generate a new selection
            // generate a valid match selection

            this.swapBlocks(scene, allBlocksInGrid, grid);
            console.log("Swapped");
            return this.getSelectable(scene, grid);
        }
    }

    getAllBlocks(grid) {
        let allBlocksInGrid = [];
        for (let i = 0; i < grid.xLength; i++) {
            for (let j = 0; j < grid.yLength; j++) {
                let b = grid.getBlock(i, j);
                if (b != null && b !== undefined) {
                    allBlocksInGrid.push(b);
                }
            }
        }
        return allBlocksInGrid;
    }

    isAtleastOneColumn(grid) {
        let c = 0;
        for (let i = 0; i < grid.xLength; i++) {
            let obj = grid.getBlock(i, 0);
            if (obj != null && obj !== undefined)
                c++;
            if (c === 2)
                return true;

        }
        return false;
    }

    swapBlocks(scene, allBlocksInGrid, grid) {
        for (let i = 0; i < allBlocksInGrid.length-1; i++) {
            // WARNING: In theory it can take forever to change blocks
            let r = Phaser.Math.Between(0, allBlocksInGrid.length-1);
            let objSwap0 = allBlocksInGrid[i];
            let objSwap1 = allBlocksInGrid[r];

            let gx0 = objSwap0.getData('gx');
            let gy0 = objSwap0.getData('gy');
            let x0 = grid.x + (gx0 * grid.width);
            let y0 = grid.y + (gy0 * grid.height);

            let gx1 = objSwap1.getData('gx');
            let gy1 = objSwap1.getData('gy');
            let x1 = grid.x + (gx1 * grid.width);
            let y1 = grid.y + (gy1 * grid.height);

            scene.tweens.add({
                targets: objSwap0,
                y: {
                    value: y1,
                    ease: 'Expo.easeOut',
                    duration: 1000
                },
                x: {
                    value: x1,
                    ease: 'Expo.easeOut',
                    duration: 1000
                }
            });
            scene.tweens.add({
                targets: objSwap1,
                y: {
                    value: y0,
                    ease: 'Expo.easeOut',
                    duration: 1000
                },
                x: {
                    value: x0,
                    ease: 'Expo.easeOut',
                    duration: 1000
                }
            });

            objSwap0.setData('gx', gx1);
            objSwap0.setData('gy', gy1);
            objSwap0.setData('x', x1);
            objSwap0.setData('y', y1);

            objSwap1.setData('gx', gx0);
            objSwap1.setData('gy', gy0);
            objSwap1.setData('x', x0);
            objSwap1.setData('y', y0);
            grid.setBlock(objSwap0, gx1, gy1);
            grid.setBlock(objSwap1, gx0, gy0);
        }
    }

    isMatchingBlock(selection) {
        for (let i = 0; i < selection.length; i++) {
            let current = selection[i];
            //if (!this.isBlockUnmatchable(current.getData('frame'), this.unmatchableFrame)) {
                for (let j = i+1; j < selection.length; j++) {
                    let next = selection[j];
                    if (current.getData('frame') === next.getData('frame')) return true;
                }
            //}
        }
        return false;
    }

    isBlockUnmatchable(blockFrame, unmatchableFrames) {
        for (let i = 0; i < unmatchableFrames.length; i++) {
            if (blockFrame === unmatchableFrames[i])
                return true;
        }
        return false;
    }

    getFirstMatchingBlock(selection) {
        for (let i = 0; i < selection.length; i++) {
            let current = selection[i];
            if (!this.isBlockUnmatchable(current.getData('frame'), this.oneClickBlockFrame)) {
                for (let j = i+1; j < selection.length; j++) {
                    let next = selection[j];
                    if (current.getData('frame') === next.getData('frame')) return [current, next];
                }
            }
        }
        return [];
    }

    isSelectableInGrid(gameObject, selectable, grid) {
        return selectable.includes(gameObject) && grid.belongsTo(gameObject);
    }

    tween(object, {sceneObject, toX = 0, toY = 0, ease = 'Expo.easeOut', duration = 500}) {
        if (object === undefined)
            console.error("Need an object to tween.");
        else {
            sceneObject.tweens.add({
                targets: object,
                y: {
                    value: toY,
                    ease: ease,
                    duration: duration
                },
                x: {
                    value: toX,
                    ease: ease,
                    duration: duration
                }
            });
        }
    }

    getBlock(x, y) {
        return this.grid[x][y];
    }

    setBlock(block, x, y) {
        this.grid[x][y] = block;
    }

    getErrorMessage(msg) {
        switch (msg) {
            case 0:
                return "Must have a sceneObject to add objects to scene.";

            default:
                return "Undefined error";
        }
    }


    get height() {
        return this._height;
    }

    set height(value) {
        this._height = value;
    }
    get width() {
        return this._width;
    }

    set width(value) {
        this._width = value;
    }

    get yLength() {
        return this._yLength;
    }

    set yLength(value) {
        this._yLength = value;
    }
    get xLength() {
        return this._xLength;
    }

    set xLength(value) {
        this._xLength = value;
    }
    get y() {
        return this._y;
    }

    set y(value) {
        this._y = value;
    }
    get x() {
        return this._x;
    }

    set x(value) {
        this._x = value;
    }


}