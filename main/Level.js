/**
 * Takes care of resources like amount of moves left etc
 * Everything regarding a level.
 */
class Level {
    constructor(levelData, levelNumb, nextScore, nextLevelRatio) {
        levelNumb -= 1;
        this._nextScore = nextScore;
        this._nextLevelRatio = nextLevelRatio;
        this._moves = levelData.level[levelNumb].moves;
        this._level = levelData.level[levelNumb].level;
        this._score = 0;
        this._levelScore = levelData.level[levelNumb].score;
        this._levelMoves = levelData.level[levelNumb].moves;
        this._blockPercentage = levelData.level[levelNumb].block.percentage;
        this._grid = levelData.level[levelNumb].grid;
    }

    createGrid(scene, yLength, x, y) {
        this.length = yLength;
        this.profileImg = scene.add.sprite(x, y, 'fbProfilePic');
        this.profileImg.setDisplaySize(scene.gg.width, scene.gg.height);
        this.profileFrame = scene.add.sprite(this.profileImg.x, this.profileImg.y, 'blocks', 9);
        scene.setScaleRatio(this.profileFrame, config.scaleRatio);


        
        this.frame = scene.add.group();
        this.frame.add(this.profileImg);
        this.frame.add(this.profileFrame);
        //this.frame.add(this.profileText);

        /*
        this.frame.getChildren().forEach(function (child) {
           child.y = 200;
        });
        */

        return this;
    }

    get nextLevelRatio() {
        return this._nextLevelRatio;
    }

    set nextLevelRatio(value) {
        this._nextLevelRatio = value;
    }
    get nextScore() {
        return this._nextScore;
    }

    set nextScore(value) {
        this._nextScore = value;
    }
    get moves() {
        return this._moves;
    }

    set moves(value) {
        this._moves = value;
    }

    get score() {
        return this._score;
    }

    set score(value) {
        this._score = value;
    }

    get level() {
        return this._level;
    }

    set level(value) {
        this._level = value;
    }

    get grid() {
        return this._grid;
    }

    set grid(value) {
        this._grid = value;
    }
    get blockPercentage() {
        return this._blockPercentage;
    }
    get levelScore() {
        return this._levelScore;
    }

    set levelScore(value) {
        this._levelScore = value;
    }

    get levelMoves() {
        return this._levelMoves;
    }

    set levelMoves(value) {
        this._levelMoves = value;
    }


}