/*let SCREEN_WIDTH = 180;
let SCREEN_HEIGHT = 360;
let BLOCK_WIDTH = 9;
let BLOCK_HEIGHT = 9;
let GRID_WIDTH = 16;
let GRID_HEIGHT = 16;
let GRID_START_Y = 64;
let GRID_START_X = 32;*/
const fb_INSTANT = false;
let fb_CONTEXT_ID = undefined;
let fb_CONTEXT_TYPE = undefined;

let fb_PLAYER_NAME = undefined;
let fb_PLAYER_PIC = undefined;
let fb_PLAYER_ID = undefined;

let game;



let config =  {
    type: Phaser.CANVAS,
    width: 160,
    height: 320,
    pixelArt: true,
    backgroundColor: '#443143', /*1b2632*/
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y : 1000 },
        }
    },

    scene: [ start, popWindow ]
};

function resize() {
    // let canvas = document.querySelector("canvas");
    let width = window.innerWidth;
    let height = window.innerHeight;
    let wratio = width / height;
    let ratio = config.width / config.height;

    if (wratio > ratio) {
        /*canvas.style.width = width + "px";
        canvas.style.height = (width / ratio) + "px";*/
        config.width = width;
        //config.height = (width / ratio);
        config.height = height;
    } else {
        /*canvas.style.width = (height * ratio) + "px";
        canvas.style.height = height + "px";*/

        //config.width = (height * ratio);
        config.width = width;
        config.height = height;
    }

}

/*function resizePhaser() {
    let windowWidth = window.innerWidth * window.devicePixelRatio;
    let windowHeight = window.innerHeight * window.devicePixelRatio;

    if(windowWidth > windowHeight){
        windowWidth = windowHeight / 1.8;
    }
    let gameWidth = windowWidth * config.height / windowHeight;
    config.width = gameWidth;

    let canvas_width = window.innerWidth * window.devicePixelRatio;
    let canvas_height = window.innerHeight * window.devicePixelRatio;
    let aspect_ratio = canvas_width / canvas_height;
    config.scaleRatio = aspect_ratio;
}*/

if (fb_INSTANT) {
    window.onload = () => {
        FBInstant.initializeAsync()
            .then(function() {
                FBInstant.setLoadingProgress(100);
                FBInstant.startGameAsync()
                    .then(function() {
                        // Retrieving context and player information can only be done
                        // once startGameAsync() resolves

                        fb_CONTEXT_ID = FBInstant.context.getID();
                        fb_CONTEXT_TYPE = FBInstant.context.getType();

                        fb_PLAYER_NAME = FBInstant.player.getName();
                        fb_PLAYER_PIC = FBInstant.player.getPhoto();
                        fb_PLAYER_ID = FBInstant.player.getID();

                        // Once startGameAsync() resolves it also means the loading view has
                        // been removed and the user can see the game viewport
                        /*
                        let windowWidth = window.innerWidth * window.devicePixelRatio;
                        let windowHeight = window.innerHeight * window.devicePixelRatio;

                        if(windowWidth > windowHeight){
                            windowWidth = windowHeight / 1.8;
                        }
                        let gameWidth = windowWidth * config.height / windowHeight;
                        config.width = gameWidth;
                        */

                        run();

                    });
            });
    }
} else {
    run();
}

function run () {
    config.scaleRatio = window.innerWidth  / config.width;
    if (config.scaleRatio >= 3.6) config.scaleRatio = 3.6;

    resize();
    window.addEventListener("resize",resize,false);
    game = new Phaser.Game(config);
    //resizePhaser();
}

/*function getScaleRatio() {
    let canvas_width = window.innerWidth * window.devicePixelRatio;
    let canvas_height = window.innerHeight * window.devicePixelRatio;
    let aspect_ratio = canvas_width / canvas_height;
    let scaleRatio = 1;
    if (aspect_ratio > 1) {
        scaleRatio = canvas_height / config.height;
    }
    else {
        scaleRatio = canvas_width / config.width;
    }
    return scaleRatio;
}*/


