class start extends Phaser.Scene {

    constructor() {

        super({ key: 'start'});

        //this.matter.world.setBounds(0, 0, config.width, config.height, 32, true, true, true, true);
        this.state = {
            handle: true,
            clickable: true,
            powerup: false,
            won: false,
            lost: false,
            tutorial: false
        };

        this.selected = [];
        this.selectable = [];

        //this.colors = [0x588C7E, 0xF2E394, 0xF2AE72, 0xD96459, 0x8C4646];
        this.lastMatchBlockFrame = null;
        this.tutorialMatchingBlocks = [];
        this.tutorialBg = null;

        this.score = 0;
        this.moves = 0;

        this.assetsLength = 5;
        if (fb_INSTANT) this.assetsLength++;
    }


    preload() {

        let assetsLoadLength = 0;
        this.load.spritesheet('blocks', 'assets/sprites/blocks64x32x16.png', { frameWidth: 16, frameHeight: 16 });
        assetsLoadLength++;

        let progress = ((assetsLoadLength)/this.assetsLength) * 100;


        this.load.json('data', 'main/data.json');
        assetsLoadLength++;

        progress = ((assetsLoadLength)/this.assetsLength) * 100;

        this.load.image('window', 'assets/sprites/window96x96.png');

        if (fb_INSTANT) {
            this.load.image('fbProfilePic', fb_PLAYER_PIC);
            assetsLoadLength++;
        }

        progress = ((assetsLoadLength)/this.assetsLength) * 100;

        //  Load the Google WebFont Loader script
        this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

        progress = ((assetsLoadLength)/this.assetsLength) * 100;

        this.load.image('flares', 'assets/sprites/pink.png');

        progress = ((assetsLoadLength)/this.assetsLength) * 100;
    }

    initGame(data, level)
    {
        level -= 1;


        this.gridLengthX = data.level[level].grid.width;
        this.gridLengthY = data.level[level].grid.height;

        this.gridWidth = 16;
        this.gridHeight = 16;
        this.gridStartY = 48;

        this.gridStartY *= config.scaleRatio;
        this.gridWidth *= config.scaleRatio;
        this.gridHeight *= config.scaleRatio;

        this.gridStartX = (this.gridWidth/2) + ((config.width / 2) - (this.gridWidth * (this.gridLengthX / 2)));
    }


    create() {
        /*
        var particles = this.add.particles('flares');
        var rect = new Phaser.Geom.Rectangle(100, 100, 600, 400);
        particles.createEmitter({

            lifespan: 1000,
            scale: { start:0.4, end: 0 },
            emitZone: { type: 'edge', source: rect, quantity: 48 }
        });
        */
        /*FBInstant
            .getLeaderboardAsync('test')
            .then(leaderboard => {
                console.log(leaderboard.getName());
                return leaderboard.setScoreAsync(42);
            })
            .then(() => console.log('Score saved'))
            .catch(error => console.error(error));*/


        /*
        FBInstant
            .getLeaderboardAsync('test')
            .then(leaderboard => leaderboard.getEntriesAsync(1, 0))
            .then(entries => {
                for (var i = 0; i < entries.length; i++) {
                    console.log(
                        entries[i].getRank() + '. ' +
                        entries[i].getPlayer().getName() + ': ' +
                        entries[i].getScore()
                    );
                }
            }).catch(error => console.error(error));
        */

        this.levelData = this.cache.json.get('data');
        // hardcore this.level, needs to be changed when saving / loading
        this.level = 1;
        this.nextScoreLevel = 800;
        this.nextScoreLevelRatio = 1.75;

        this.lvl = new Level(this.levelData, this.level, this.nextScoreLevel, this.nextScoreLevelRatio);
        this.initGame(this.levelData, this.level);


        // GRID INITIALIZER VARIABLES
        const BLOCK_WIDTH = 16;
        const BLOCK_HEIGHT = 16;
        const Y_START = 48;

        const gw = BLOCK_WIDTH * config.scaleRatio;
        const gh = BLOCK_HEIGHT * config.scaleRatio;
        const xgLen = this.levelData.level[this.level - 1].grid.width;
        const ygLen = this.levelData.level[this.level - 1].grid.height;
        const xgStart = (gw/2) + ((config.width / 2) - (gw * (xgLen / 2)));
        const ygStart = Y_START * config.scaleRatio;
        this.gg = new Grid({
            gridHeight: gw,
            gridWidth: gh,
            xGridLength: xgLen,
            yGridLength: ygLen,
            xStartPosition: xgStart,
            yStartPosition: ygStart
        }).createGrid(this, this.levelData, this.level);

        // gw needs to be changed
        this.lvl.createGrid(this, 5, gw, ygStart);

        this.pu = new Powerups().createPowers(this, config.width - gw, ygStart);

        this.txt = new TextSetup();
        this.txt.initScoreText(this, this.nextScoreLevel);
        this.txt.initMovesLeftText(this, this.levelData.level[this.level-1].moves);
        this.txt.initLevelText(this, this.level);

        this.selectable = this.gg.getSelectable(this, this.gg);


        this.window = new WindowPopup(this);
        /*
        this.highscoreWindow = this.scene.launch('popWindow', {
            width: 5,
            height: 7,
            x: xgStart,
            y: ygStart
        });
        */

        // SET TUTORIAL
        this.tutorial = this.add.sprite(config.width - gw, ygStart - (gh * 2), 'blocks', 11);
        this.tutorial.setInteractive();
        this.setScaleRatio(this.tutorial, config.scaleRatio);

        this.input.on('gameobjectdown', function (pointer, gameObject) {
            /*if (this.state.won) {
                this.handleWin(gameObject);
            } else*/

            if (this.state.lost) {
                this.handleLost(gameObject);
                console.log("You've lost!");
            } else if (this.state.tutorial) {
                this.handleTutorial(gameObject);
            } else if (this.state.handle) {
                this.handleGrid(gameObject);
                this.handlePowerup(gameObject);
            } else if (this.state.powerup) {
                this.handleGridOnPowerup(gameObject);
            }

        }, this);



    }




    update(elapsed, delta) {

    }

    handleLost(gameObject) {
        if (gameObject === this.window.windowLost) {
            this.state.lost = false;
            this.window.disappearLostWindow(this);
            this.gg.resetLevel();
            this.gg.createGrid(this, this.levelData, this.level);
            this.selectable = this.gg.getSelectable(this, this.gg);

            this.lvl.score = 0;
            this.txt.updateScoreText(this.lvl.score, this.lvl.nextScore);

            this.lvl.moves = this.levelData.level[this.level-1].moves;
            this.txt.updateMovesLeftText(this.lvl.moves);

            this.lastMatchBlockFrame = null;
        }
    }

    handleTutorial(gameObject) {
        if (gameObject === this.tutorialMatchingBlocks[0] || gameObject === this.tutorialMatchingBlocks[1]) {
            // If there's a match 2:
            if (this.handleGrid(gameObject)) {
                this.tutorialBg.destroy();
                this.state.tutorial = false;
                this.tutorialMatchingBlocks = [];
            }
        }
    }

/*
    handleWin(gameObject) {
        if (gameObject === this.window.windowWon) {
            this.window.disappearWonWindow(this);
            this.gg.resetLevel();
            this.gg.createGrid(this, this.levelData, ++this.level);
            this.state.won = false;
            this.selectable = this.gg.getSelectable(this, this.gg);
            this.txt.updateScoreText(0);
            this.txt.updateMovesLeftText(this.levelData.level[this.level-1].moves)
            this.lvl.moves = this.levelData.level[this.level-1].moves;
            this.score = 0;
            this.lastMatchBlockFrame = null;
        }
    }
*/
    /**
     *  activateTutorial
     * @returns {boolean} Can be activated
     */
    activateTutorial() {
        this.tutorialMatchingBlocks = this.gg.getFirstMatchingBlock(this.selectable);
        if (this.tutorialMatchingBlocks.length < 2)
            return false;

        this.tutorialMatchingBlocks[0].setDepth(100);
        this.tutorialMatchingBlocks[1].setDepth(100);

        this.tutorialBg = this.add.graphics();

        this.tutorialBg.fillStyle("#000000", 0.6);
        this.tutorialBg.fillRect(0, 0, config.width, config.height);

        return true;
    }

    handleGridOnPowerup(gameObject) {
        // If clicked on another powerup
        if (this.pu.belongsTo(gameObject)) {
            // Implemented "selection"
            this.pu.lastSelected = gameObject;
        } else if (this.gg.belongsTo(gameObject)) {
            switch(this.pu.lastSelected.getData('type')) {
                case "wheel":
                    this.pu.usePower(this, { powerup : { wheel: true}, column: gameObject.getData('gx') });
                    break;
                case "dynamite":
                    this.pu.usePower(this, { powerup : { dynamite: true }, column: gameObject.getData('gx'), row: gameObject.getData('gy') });
                    break;
                case "remover":
                    this.pu.usePower(this, { powerup : { remover: true }, column: gameObject.getData('gx'), row: gameObject.getData('gy') });
                    break;

                default:
                    break;
            }

            this.gg.checkAddRow(this);
            this.selectable = this.gg.getSelectable(this, this.gg);
            if (this.selectable.length === 0) {
                this.state.lost = true;
                this.window.appearLostWindow(this);
            }
        }

        this.pu.removeArrows();
        this.changeState("fromPowerupToHandle");
    }

    checkNextLevel() {
        if (this.lvl.score >= this.lvl.nextScore) {
            this.lvl.nextScore = Math.floor(this.lvl.nextScore * this.lvl.nextLevelRatio);
            this.txt.updateScoreText(this.lvl.score, this.lvl.nextScore);
            this.txt.updateLevelText(++this.lvl.level);
        }
    }

    changeState(description) {
        switch (description) {
            case "fromHandleToPowerup":
                this.state.handle = false;
                this.state.powerup = true;
                break;

            case "fromPowerupToHandle":
                this.state.powerup = false;
                this.state.handle = true;

            default:
                break;
        }
    }

    handlePowerup(gameObject) {
        if (this.pu.belongsTo(gameObject)) {
            this.changeState("fromHandleToPowerup");
            this.pu.lastSelected = gameObject;
            // Here we need to fly some arrows
            this.pu.createArrows(this, this.gridStartX, config.height);

        }
    }

    /***
     * Return true for match return false for no match
     * @param gameObject
     */
    handleGrid(gameObject) {
        let matched = false;
        // Get all the selectables first
        this.selectable = this.gg.getSelectable(this, this.gg);

        // Check if we want the tutorial to tick off
        if (gameObject === this.tutorial && !this.state.tutorial)
        {
            if (this.activateTutorial())
                this.state.tutorial = true;
            return;
        }

        // If the selected object is a grid block
        if (this.gg.isSelectableInGrid(gameObject, this.selectable, this.gg)) {

            // If block is not included of the selected blocks
            if (!this.selected.includes(gameObject)) {

                // If it's one block already selected
                if (this.selected.length > 0) {

                    // If it's the same color we have a 2 match
                    if (this.selected[0].getData('frame') === gameObject.getData('frame')) {
                        matched = true;
                        this.selected.push(gameObject);

                        this.updateScore();

                        this.lastMatchBlockFrame = gameObject.getData('frame');

                        this.updateMove();

                        this.setAlpha(this.selected, 1);

                        // Remove from grid
                        for (let i = 0; i < this.selected.length; i++) {
                            this.gg.removeBlock(this.selected[i].getData('gx'), this.selected[i].getData('gy'), {
                                sceneObject: this,
                                toY: config.height,
                                toX: this.selected[i].getData('x')
                            });
                            this.gg.checkAddRow(this);
                        }

                        // Checking for win state first before lost state
                        //this.checkWinState();

                        // Update selectable blocks and check for lost state
                        this.selectable = this.gg.getSelectable(this, this.gg);
                        this.checkLostState();
                        this.checkNextLevel();

                    } else {
                        // Unselect selection of block
                        this.setAlpha(this.selected, 1);
                    }

                    // Remove selected blocks
                    this.selected = [];

                } else {
                    
                    // If the block selected is a bonus block: 10 equals to add moves
                    if (gameObject.getData('frame') === 10) {
                        this.gg.removeBlock(gameObject.getData('gx'), gameObject.getData('gy'), {
                            sceneObject: this,
                            toY: config.height,
                            toX: gameObject.getData('x')
                        });
                        this.gg.checkAddRow(this);
                        this.lvl.moves += 4;
                        this.txt.updateMovesLeftText(this.lvl.moves);
                    } else {
                        // If there's no selected blocks
                        this.selected.push(gameObject);
                        this.setAlpha(this.selected, 0.5);
                    }

                }

            } else {
                // If block is included then we select the same block again,
                // which means we can remove the selection
                this.setAlpha(this.selected, 1);

                // Remove selected blocks
                this.selected = [];
            }

        } else {
            // Remove selectables
            this.setAlpha(this.selected, 1);
            this.selected = [];
        }
        return matched;
    }

    checkWinState() {
        if (this.lvl.score >= this.levelData.level[this.level - 1].score) {
            this.state.won = true;
            this.window.appearWonWindow(this);
        }
    }

    checkLostState() {
        if ((this.selectable.length === 0 || this.lvl.moves === 0) && !this.state.won) {
            this.state.lost = true;
            this.window.appearLostWindow(this);
        }
    }

    updateScore() {
        if (this.selected[0].getData('frame') === this.lastMatchBlockFrame) {
            this.lvl.score += 125 * this.lvl.level;
        } else {
            this.lvl.score += 45 * this.lvl.level;
        }
        this.txt.updateScoreText(this.lvl.score, this.lvl.nextScore);
    }

    updateMove() {
        this.txt.updateMovesLeftText(--this.lvl.moves);
    }


    /**
     * Sets the sprites alpha of the whole array
     * @param arrSprite - An array which inludes sprites
     * @param alpha - set the sprites alpha value
     */
    setAlpha(arrSprite, alpha) {
        for (let i = 0; i < arrSprite.length; i++) {
            if (arrSprite[i].alpha != alpha) {
                arrSprite[i].alpha = alpha;
            }
        }
    }

    setTint(arrSprite, tint) {
        for (let i = 0; i < arrSprite.length; i++) {
            if (arrSprite[i].tint != tint) {
                arrSprite[i].tint = tint;
            }
        }
    }

    clickHandler (block)
    {
        // to invoke this
        // gameObject.emit('clicked', gameObject);
    }

    updateScaleRatio() {

    }

    setScaleRatio(sprite, amount) {
        sprite.scaleX = amount;
        sprite.scaleY = amount;
    }



}