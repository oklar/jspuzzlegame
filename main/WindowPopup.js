class WindowPopup {
    constructor(scene) {
        //this.scene = scene;
        this.windowLostGroup = scene.add.group();
        this.windowLost = scene.add.sprite(config.width/2, scene.gridStartY + scene.gridHeight*4, 'window');
        this.windowLostGroup.add(this.windowLost);
        this.windowLost.depth = 1000;
        this.windowLost.setInteractive();
        scene.setScaleRatio(this.windowLost, 0);
        //this.windowLostRetryBtn = scene.add.sprite(config.width/2, scene.gridStartY + scene.gridHeight*4, 'window');


        this.windowWonGroup = scene.add.group();
        this.windowWon = scene.add.sprite(config.width/2, scene.gridStartY + scene.gridHeight*4, 'window');
        this.windowWonGroup.add(this.windowWon);
        this.windowWon.depth = 1000;
        this.windowWon.setInteractive();
        scene.setScaleRatio(this.windowWon, 0);
    }

    appearLostWindow(scene) {
        scene.tweens.add({
            targets: this.windowLost,
            scaleX: {
                value: config.scaleRatio,
                ease: 'Bounce.easeOut',
                duration: 1000
            },
            scaleY: {
                value: config.scaleRatio,
                ease: 'Bounce.easeOut',
                duration: 1000
            }
        });
    }

    appearWonWindow(scene) {
        scene.tweens.add({
            targets: this.windowWon,
            scaleX: {
                value: config.scaleRatio,
                ease: 'Bounce.easeOut',
                duration: 1000
            },
            scaleY: {
                value: config.scaleRatio,
                ease: 'Bounce.easeOut',
                duration: 1000
            }
        });
    }

    disappearLostWindow(scene) {
        scene.tweens.add({
            targets: this.windowLost,
            scaleX: {
                value: 0,
                ease: 'Bounce.easeIn',
                duration: 1000
            },
            scaleY: {
                value: 0,
                ease: 'Bounce.easeIn',
                duration: 1000
            }
        });
    }

    disappearWonWindow(scene) {
        scene.tweens.add({
            targets: this.windowWon,
            scaleX: {
                value: 0,
                ease: 'Bounce.easeIn',
                duration: 1000
            },
            scaleY: {
                value: 0,
                ease: 'Bounce.easeIn',
                duration: 1000
            }
        });
    }

}