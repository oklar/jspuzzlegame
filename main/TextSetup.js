class TextSetup {
    constructor() {
        this.scoreText;
        this.movesLeftText;
        this.levelText;
    }

    initScoreText(scene, nextScore) {
        this.scoreText = scene.add.text(16, 16, `Score: 0 / ${nextScore}`).
            setAlign('center').setFontFamily('"Press Start 2P"').setFontSize(10 * (config.scaleRatio/1.5))
            .setColor('#ebe2ff')
            .setStroke('#2d2e43', 10);
    }

    initLevelText(scene, level) {
        this.levelText = scene.add.text(16, scene.gg.y + (scene.gg.height/2) + 6, 'Lvl: ' + level).
        setAlign('center').setFontFamily('"Press Start 2P"').setFontSize(4 * (config.scaleRatio/1.5))
            .setColor('#ebe2ff')
            .setStroke('#2d2e43', 10);
    }

    initMovesLeftText(scene, moves) {
        this.movesLeftText = scene.add.text(16, 48, `Moves: ${moves}`).
            setAlign('center').setFontFamily('"Press Start 2P"').setFontSize(10 * (config.scaleRatio/1.5))
            .setColor('#ebe2ff')
            .setStroke('#2d2e43', 10);
    }

    updateLevelText(level) {
        this.levelText.text = `Lvl: ${level}`;
        this.levelText.updateText();
    }

    updateScoreText(score, nextScore) {
        this.scoreText.text = `Score: ${score} / ${nextScore}`;
        this.scoreText.updateText();
    }

    updateMovesLeftText(moves) {
        this.movesLeftText.text = `Moves: ${moves}`;
        this.movesLeftText.updateText();
    }
}