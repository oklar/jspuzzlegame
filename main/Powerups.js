class Powerups {
    constructor({wheel = 0, dynamite = 0, remover = 0, imgWidth = 16, imgHeight = 16} = {}) {
        this.wheelNumb = wheel;
        this.dynamiteNumb = dynamite;
        this.removerNumb = remover;
        this.imgWidth = imgWidth;
        this.imgHeight = imgHeight;
        this.powers = [];
        this.greenArrows = [];

        this.lastSelected = null;
    }

    createPowers(sceneObject, x, y) {
        this.wheel = sceneObject.add.sprite(x, y, 'blocks', 4);
        sceneObject.setScaleRatio(this.wheel, config.scaleRatio);
        this.wheel.setInteractive();
        this.wheel.setData('amount', this.wheelNumb);
        this.wheel.setData('type', "wheel");
        this.powers.push(this.wheel);

        this.dynamite = sceneObject.add.sprite(x, y + (sceneObject.gg.height * 1.5), 'blocks', 6);
        sceneObject.setScaleRatio(this.dynamite, config.scaleRatio);
        this.dynamite.setInteractive();
        this.dynamite.setData('amount', this.dynamiteNumb);
        this.dynamite.setData('type', "dynamite");
        this.powers.push(this.dynamite);

        this.remover = sceneObject.add.sprite(x, y + (sceneObject.gg.height * 3), 'blocks', 7);
        sceneObject.setScaleRatio(this.remover, config.scaleRatio);
        this.remover.setInteractive();
        this.remover.setData('amount', this.removerNumb);
        this.remover.setData('type', "remover");
        this.powers.push(this.remover);

        return this;
    }

    createArrows(sceneObject, x, y) {
        for (let i = 0; i < sceneObject.gg._xLength; i++) {
            let columnLength = sceneObject.gg.getColumnLength(i);
            if (columnLength !== -1) {
                let ga = sceneObject.add.sprite(x + (i * sceneObject.gg.width), y, 'blocks', 5);
                sceneObject.setScaleRatio(ga, config.scaleRatio);
                sceneObject.tweens.add({
                    targets: ga,
                    y: {
                        value: sceneObject.gridStartY + ((columnLength + 0.5) * sceneObject.gg.height),
                        ease: 'Bounce.easeOut',
                        duration: 1000
                    }
                });
                this.greenArrows.push(ga);
            }

        }
    }

    removeArrows() {
        for (let i = 0; i < this.greenArrows.length; i++) {
            let obj = this.greenArrows[i];
            obj.destroy();
        }
        this.greenArrows = [];
    }

    belongsTo(gameObject) {
        return this.powers.includes(gameObject);
    }

    usePower(sceneObject, {powerup : { wheel, dynamite, remover }, column = 0, row = 0 }) {
        if (wheel) {
            this.useWheel(sceneObject, column);
        } else if (dynamite) {
            this.useDynamite(sceneObject, column, row);
        } else if (remover) {
            this.useRemover(sceneObject, column, row);
        }
    }

    useWheel(sceneObject, column, effects = true) {
        this.useRemover(sceneObject, column, 0);
        /*
        for (let i = sceneObject.gg.getColumnLength(column); i > -1; i--) {
            let b = sceneObject.gg.getBlock(column, i);
            if (b != null && b !== undefined) {
                sceneObject.gg.removeBlock(column, i, {
                    sceneObject: sceneObject,
                    toX: b.getData('x'),
                    toY: config.height
                });
            }
        }
        */
    }

    useDynamite(sceneObject, column, row) {
        // Get that column height
        let h = sceneObject.gg.getColumnLength(column);
        // Remove all in the column
        this.useWheel(sceneObject, column, false);
        // Then remove all from the row
        for (let i = 0; i < sceneObject.gg.xLength; i++) {
            let b = sceneObject.gg.getBlock(i, h);
            if (b != null && b !== undefined) {
                sceneObject.gg.removeBlock(i, h, {
                    sceneObject: sceneObject,
                    toX: b.getData('x'),
                    toY: config.height
                });
            }
        }
    }

    useRemover(sceneObject, column, row) {
        // Remove any items including rock from x & (y pos and downwards)
        let b = sceneObject.gg.getBlock(column, row);
        if (b != null && b !== undefined) {
            sceneObject.gg.removeBlock(column, row, {
                sceneObject: sceneObject,
                toX: b.getData('x'),
                toY: config.height
            });
        }
    }
}