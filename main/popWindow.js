class popWindow extends Phaser.Scene {
    constructor() {
        super({ key: 'popWindow'});
        this.assetLength = 1;
        this.fWidth = 16; // the actual pixel size of a block
        this.fWidthScale = this.fWidth * config.scaleRatio;
        this.maxYScroll = -1000;
        this.move = false;
        this.yOnDown = 0;
    }

    preload(){
        let assetsLoadLength = 0;
        let progress = ((assetsLoadLength)/this.assetsLength) * 100;
        this.load.spritesheet('hs_windowblocks', 'assets/sprites/hscore_windowblock48x48x16.png', { frameWidth: 16, frameHeight: 16 });
        assetsLoadLength++;

        progress = ((assetsLoadLength)/this.assetsLength) * 100;
    };

    create(data){
        this.highscoreContainer = this.add.container(0, 0);
        this.highscoreContainer.x = data.x;
        this.highscoreContainer.y = data.y;

        this.createWindow(this.highscoreContainer, {
            width:  data.width,
            height:  data.height,
            x: 0,
            y: 0
        });

        this.insideHighscoreContainer = this.add.container(0, 0);
        this.insideHighscoreContainer.x = data.x + this.fWidthScale;
        this.insideHighscoreContainer.y = data.y + this.fWidthScale;
        this.createWindow(this.insideHighscoreContainer, {
            width:  3,
            height:  1,
            x: 0,
            y: 0
        });
        this.createWindow(this.insideHighscoreContainer, {
            width:  3,
            height:  1,
            x: 0,
            y: this.fWidthScale * 2
        });

        this.addMaskToContainer(this.insideHighscoreContainer, {
            x1: data.x,
            y1: data.y - 7,
            x2: (data.width * this.fWidthScale),
            y2: (data.height * this.fWidthScale) - 50,
        });

        this.input.on('pointerdown', (pointer) => {
            this.yOnDown = this.insideHighscoreContainer.y - pointer.y;
            //this.timer = new Date();
            this.move = true;
        });

        this.input.on('pointerup', (pointer) => {
            this.move = false;
            //let deltaTime = new Date() - this.timer;
        });

        this.input.on('pointermove', (pointer) => {
            if (this.move) {
                this.insideHighscoreContainer.y = (this.yOnDown + pointer.y);
            }
        });


    };

    update(){

    };

    createWindow(container, containerConfig) {
        let wWidth = containerConfig.width * this.fWidthScale;
        let wHeight = containerConfig.height * this.fWidthScale;

        let amountBlockWidth = Math.floor(wWidth / (this.fWidthScale));
        let amountBlockHeight = Math.floor(wHeight / (this.fWidthScale));

        let cPosX = amountBlockWidth * this.fWidthScale;
        let cPosY = amountBlockHeight * this.fWidthScale;

        let wCornerUL = this.add.sprite(containerConfig.x, containerConfig.y, 'hs_windowblocks', 0);
        let wCornerUR = this.add.sprite(containerConfig.x + cPosX, containerConfig.y, 'hs_windowblocks', 2);
        let wCornerDL = this.add.sprite(containerConfig.x, containerConfig.y + cPosY, 'hs_windowblocks', 6);
        let wCornerDR = this.add.sprite(containerConfig.x + cPosX, containerConfig.y + cPosY, 'hs_windowblocks', 8);

        this.setScaleRatio(wCornerUL, config.scaleRatio);
        this.setScaleRatio(wCornerUR, config.scaleRatio);
        this.setScaleRatio(wCornerDL, config.scaleRatio);
        this.setScaleRatio(wCornerDR, config.scaleRatio);

        container.add([wCornerDL, wCornerDR, wCornerUL, wCornerUR]);

        for (let i = 0; i < amountBlockWidth - 1; i++) {
            let upBlock = this.add.sprite(containerConfig.x + this.fWidthScale + (i * this.fWidthScale), containerConfig.y, 'hs_windowblocks', 1);
            let downBlock = this.add.sprite(containerConfig.x + this.fWidthScale + (i * this.fWidthScale), containerConfig.y + cPosY, 'hs_windowblocks', 7);
            this.setScaleRatio(upBlock, config.scaleRatio);
            this.setScaleRatio(downBlock, config.scaleRatio);
            container.add([upBlock, downBlock]);
        }

        for (let i = 0; i < amountBlockHeight - 1; i++) {
            let leftSide = this.add.sprite(containerConfig.x, containerConfig.y + this.fWidthScale + (i * this.fWidthScale), 'hs_windowblocks', 3);
            let rightSide = this.add.sprite(containerConfig.x + cPosX, containerConfig.y + this.fWidthScale + (i * this.fWidthScale), 'hs_windowblocks', 5);
            this.setScaleRatio(leftSide, config.scaleRatio);
            this.setScaleRatio(rightSide, config.scaleRatio);
            container.add([leftSide, rightSide]);
        }
    }

    addMaskToContainer(container, maskConfig) {
        let graphics = this.add.graphics();
        let color = 0xff0000;
        let alpha = 0.25;
        graphics.fillStyle(color, alpha);
        graphics.fillRect(maskConfig.x1, maskConfig.y1, maskConfig.x2, maskConfig.y2);
        container.mask = new Phaser.Display.Masks.GeometryMask(this, graphics);
    }

    setScaleRatio(sprite, amount) {
        sprite.scaleX = amount;
        sprite.scaleY = amount;
    }
}